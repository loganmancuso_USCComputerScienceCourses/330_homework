% This is the Prolog version of the family example

child(john,sue).   child(john,sam).   
child(jane,sue).   child(jane,sam).   
child(sue,george).   child(sue,gina). 

male(john).   male(sam).     male(george). 
female(sue).  female(jane).  female(june). 

parent(Y,X) :- child(X,Y).
father(Y,X) :- child(X,Y), male(Y).
opp_sex(X,Y) :- male(X), female(Y). 
opp_sex(Y,X) :- male(X), female(Y). 
sibling(X,Y) :- parent(W,X), parent(Z,X), 
											parent(W,Y), parent(Z,Y),
											\+Z=W, \+X=Y.
grand_father(X,Z) :- father(X,Y), parent(Y,Z).
grand_parent(X,Y) :- parent(Z,Y), parent(Y,X).
%child X and Y share both parents W and Z
full_sibling(X,Y) :- parent(W,X), parent(Z,X), parent(W,Y), parent(Z,Y), \+Z=W, \+X=Y.
%X has a parent Z, who has a parent W, who has a parent H,
%who has a sibling K, who has a child Y
first_cousin_twice_removed(X,Y) :- parent(Z,X), parent(W,Z), parent(H,W), full_sibling(H,K), child(Y,K), \+X=Y, \+K=W, \+H=K, \+Z=H, \+W=Z, \+Z=K.  
