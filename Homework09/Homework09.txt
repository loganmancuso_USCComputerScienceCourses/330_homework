Chapter 2
1)

Script started on Tue 07 Nov 2017 03:41:19 PM STD
# Custom Functions Loaded
# Author/CopyRight: Mancuso, Logan
LWKS-SBook~\Homework\ >> 2+3*4
2+3*4: command not found
LWKS-SBook~\Homework\ >> ghci
GHCi, version 7.6.3: http://www.haskell.org/ghc/  :? for help
Loading package ghc-prim ... linking ... done.
Loading package integer-gmp ... linking ... done.
Loading package base ... linking ... done.
Prelude> 2+3*4
14
Prelude> (2+3)*4
20
Prelude> sqrt (3^2 + 4^2)
5.0
Prelude> head [1,2,3,4,5]
1
Prelude> tail [1,2,3,4,5]
[2,3,4,5]
Prelude> [1,2,3,4,5] !! 2
3
Prelude> take 3 [1,2,3,4,5]
[1,2,3]
Prelude> drop 3 [1,2,3,4,5]
[4,5]
Prelude> length [1,2,3,4,5]6
5
Prelude> sum [1,2,3,4,5]
15
Prelude> product [1,2,3,4,5]
120
Prelude> [1,2,3] ++ [4,5}

<interactive>:13:16: parse error on input `}'
Prelude> [1,2,3] ++ [4,5]
[1,2,3,4,5]
Prelude> reverse [1,2,3,4,5]
[5,4,3,2,1]
Prelude> :quit
Leaving GHCi.
LWKS-SBook~\Homework\ >> exit
exit

Script done on Tue 07 Nov 2017 03:43:41 PM STD

2)

(2^3) * 4
(2*3) + (4*5)
(2+3) * (4^5)

4)

tail' :: [a] -> a
tail' (x:xs)
	null xs   = x
 	otherwise = tail' xs

5)
init' :: [a] -> [a]
init' (x:xs)
	null xs = []
	otherwise = x : (init' xs)

Chapter 3

3)
second :: [a] -> a
second xs = head (tail xs )

swap :: (a, b) -> (b, a)
swap (x, y) = (y, x)

pair :: a -> b -> (a, b)
pair x y = (x, y)

double :: Num a => a -> a
double x = x*2

palindrome :: Eq a => [a] -> Bool
palindrome xs = reverse xs == xs

twice :: (a -> a) -> a -> a
twice f x = f (f x)

4)

Script started on Tue 07 Nov 2017 03:59:16 PM STD
# Custom Functions Loaded
# Author/CopyRight: Mancuso, Logan
LWKS-SBook~\Homework09\ >> ghci
GHCi, version 7.6.3: http://www.haskell.org/ghc/  :? for help
Loading package ghc-prim ... linking ... done.
Loading package integer-gmp ... linking ... done.
Loading package base ... linking ... done.
Prelude> second [1,2,3,4.5]

<interactive>:2:1: Not in scope: `second'
Prelude> second [1,2,3,4.5]
Prelude> :quit
Leaving GHCi.
LWKS-SBook~\Homework09\ >> nano tmp.hs
GHCi, version 7.6.3: http://www.haskell.org/ghc/  :? for help
Loading package ghc-prim ... linking ... done.
Loading package integer-gmp ... linking ... done.
Loading package base ... linking ... done.
[1 of 1] Compiling Main 	    ( tmp.hs, interpreted )
Ok, modules loaded: Main.
*Main> second [1,2,3,4,5]
2
*Main> swap [1,2,3,4,5]
(2,1)
*Main> swap ([1,2,3],[3,4,5])
([3,4,5],[1,2,3])
*Main> pair(1 2
(1,2)
*Main> double 2
4
*Main> palindrome [1,2,3,4,3,2,1]
True
*Main> twice (1,2)
Leaving GHCi.
LWKS-SBook~\Homework09\ >> exit
exit

Script done on Tue 07 Nov 2017 04:02:48 PM STD
